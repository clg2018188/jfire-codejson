package com.jfireframework.codejson.methodinfo.impl.write.array;

import java.lang.reflect.Method;
import com.jfireframework.codejson.function.WriteStrategy;
import com.jfireframework.codejson.methodinfo.MethodInfoBuilder;
import com.jfireframework.codejson.methodinfo.impl.write.AbstractWriteMethodInfo;
import com.jfireframework.codejson.util.NameTool;

public abstract class AbstractWriteArrayMethodInfo extends AbstractWriteMethodInfo
{
    protected Method method;
    
    public AbstractWriteArrayMethodInfo(Method method, WriteStrategy strategy, String entityName)
    {
        super(method, strategy, entityName);
        this.method = method;
        Class<?> returnType = method.getReturnType();
        int dim = NameTool.getDimension(returnType);
        Class<?> rootType = NameTool.getRootType(returnType);
        String rootName = rootType.getName();
        str = "if(" + getValue + " != null)\r\n{\r\n";
        String key = method.getDeclaringClass().getName() + '.' + NameTool.getNameFromMethod(method, strategy);
        String arrayName = "array" + dim;
        String fieldName = NameTool.getNameFromMethod(method, strategy);
        str += "\t" + NameTool.buildDimTypeName(rootName, dim) + ' ' + arrayName + " = " + getValue + ";\r\n";
        str += "\tcache.append(\"\\\"" + NameTool.getNameFromMethod(method, strategy) + "\\\":\");\r\n";
        if (strategy != null && strategy.containsStrategyField(key))
        {
            if (strategy.isUseTracker())
            {
                str += "\t_$tracker.reset(_$reIndex);\r\n";
                str += "\tint _$index = _$tracker.indexOf(" + arrayName + ");\r\n";
                str += "\tif(_$index == -1)\r\n";
                str += "\t{\r\n";
                str += "\t\t_$tracker.put(" + arrayName + ",\"" + fieldName + "\",false);\r\n";
                str += "\t}\r\n";
                str += "\twriteStrategy.getWriterByField(\"" + key + "\").write(" + arrayName + ",cache," + entityName + ",_$tracker);\r\n";
            }
            else
            {
                str += "\twriteStrategy.getWriterByField(\"" + key + "\").write(" + arrayName + ",cache," + entityName + ",null);\r\n";
                str += "\tcache.append(',');\r\n";
                str += "}\r\n";
            }
        }
        else
        {
            String bk = "\t";
            String nextBk = "\t\t";
            if (strategy != null && strategy.isUseTracker())
            {
                str += "\t_$tracker.reset(_$reIndex);\r\n";
                str += "\tint _$index = _$tracker.indexOf(" + arrayName + ");\r\n";
                str += "\tif(_$index != -1)\r\n";
                str += "\t{\r\n";
                str += "\t\tJsonWriter writer = writeStrategy.getTrackerType(" + arrayName + ".getClass());\r\n";
                str += "\t\tif(writer != null)\r\n";
                str += "\t\t{\r\n";
                str += "\t\t\twriter.write(" + arrayName + ",cache," + entityName + ",_$tracker);\r\n";
                str += "\t\t}\r\n";
                str += "\t\telse\r\n";
                str += "\t\t{\r\n";
                str += "\t\t\tcache.append(\"{\\\"$ref\\\":\\\"\").append(_$tracker.getPath(_$index)).append('\"').append('}');\r\n";
                str += "\t\t}\r\n";
                str += "\t\tcache.append(',');\r\n";
                str += "\t}\r\n";
                str += "\telse\r\n";
                str += "\t{\r\n";
                str += "\t\tint _$reIndexarray" + dim + " = _$tracker.put(" + arrayName + ",\"" + fieldName + "\",false);\r\n";
                bk = "\t\t";
                nextBk = "\t\t\t";
            }
            if (strategy != null && (MethodInfoBuilder.wrapperSet.contains(rootType) || (rootType.isPrimitive() && strategy.containsStrategyType(rootType))))
            {
                str += bk + "JsonWriter baseWriter = writeStrategy.getWriter(" + rootType.getName() + ".class);\r\n";
            }
            for (int i = 0; i < dim; i++)
            {
                String pre = "array" + (dim - i + 1);
                String preIndex = "_$reIndex" + pre;
                String now = "array" + (dim - i);
                String nowIndex = "_$reIndex" + now;
                String next = "array" + (dim - i - 1);
                String writerName = "writer" + now;
                if (i != 0)
                {
                    str += bk + "if(" + now + " != null)\r\n" + bk + "{\r\n";
                    if (strategy != null && strategy.isUseTracker())
                    {
                        str += nextBk + "_$tracker.reset(" + preIndex + ");\r\n";
                        str += nextBk + "int " + nowIndex + " = _$tracker.indexOf(" + now + ");\r\n";
                        str += nextBk + "if(" + nowIndex + "!= -1)\r\n";
                        str += nextBk + "{\r\n";
                        str += nextBk + "\tJsonWriter " + writerName + " = writeStrategy.getTrackerType(" + now + ".getClass());\r\n";
                        str += nextBk + "\tif(" + writerName + " != null)\r\n";
                        str += nextBk + "\t{\r\n";
                        str += nextBk + "\t\t" + writerName + ".write(" + now + ",cache," + entityName + ",_$tracker);\r\n";
                        str += nextBk + "\t}\r\n";
                        str += nextBk + "\telse\r\n";
                        str += nextBk + "\t{\r\n";
                        str += nextBk + "\t\tcache.append(\"{\\\"$ref\\\":\\\"\").append(_$tracker.getPath(" + nowIndex + ")).append('\"').append('}');\r\n";
                        str += nextBk + "\t}\r\n";
                        str += nextBk + "}\r\n";
                        str += nextBk + "else\r\n";
                        str += nextBk + "{\r\n";
                        str += nextBk + "\t_$tracker.put(" + now + ",\"[\"+i" + (dim - i + 1) + "+']',true);\r\n";
                        nextBk += "\t";
                    }
                }
                else
                {
                    nextBk = bk;
                }
                str += nextBk + "cache.append('[');\r\n";
                String index = "i" + (dim - i);
                String lengthStr = "h" + (dim - i);
                str += nextBk + "int " + lengthStr + " = " + now + ".length;\r\n";
                str += nextBk + "for(int " + index + "=0;" + index + "<" + lengthStr + ";" + index + "++)\r\n" + nextBk + "{\r\n";
                if (i != dim - 1)
                {
                    str += nextBk + '\t' + NameTool.buildDimTypeName(rootName, dim - i - 1) + " " + next + " = " + now + "[" + index + "];\r\n";
                }
                bk = nextBk + '\t';
                nextBk = bk + '\t';
            }
            writeOneDim(rootType, bk);
            bk = bk.substring(0, bk.length() - 1);
            if (strategy != null && strategy.isUseTracker())
            {
                for (int i = dim; i > 0; i--)
                {
                    if (i == dim)
                    {
                        nextBk = bk.substring(0, bk.length() - 1);
                        str += bk + "}\r\n";
                        str += bk + "if(cache.isCommaLast()){cache.deleteLast();}\r\n";
                        str += bk + "cache.append(\"],\");\r\n";
                        str += nextBk + "}\r\n";
                        bk = nextBk.substring(0, nextBk.length() - 1);
                    }
                    else
                    {
                        str += bk + "}\r\n";
                        nextBk = bk.substring(0, bk.length() - 1);
                        str += nextBk + "}\r\n";
                        str += nextBk + "if(cache.isCommaLast()){cache.deleteLast();}\r\n";
                        str += nextBk + "cache.append(\"],\");\r\n";
                        nextBk = bk.substring(0, nextBk.length() - 1);
                        str += nextBk + "}\r\n";
                        bk = nextBk.substring(0, nextBk.length() - 1);
                    }
                }
                if (strategy != null && strategy.isUseTracker())
                {
                    str += "}\r\n";
                }
                
            }
            else
            {
                for (int i = dim; i > 0; i--)
                {
                    nextBk = bk.substring(0, bk.length() - 1);
                    str += bk + "}\r\n";
                    str += bk + "if(cache.isCommaLast()){cache.deleteLast();}\r\n";
                    str += bk + "cache.append(\"],\");\r\n";
                    str += nextBk + "}\r\n";
                    if (i != 1)
                    {
                        bk = nextBk.substring(0, nextBk.length() - 1);
                    }
                }
            }
        }
    }
    
    protected abstract void writeOneDim(Class<?> rootType, String bk);
    
}
