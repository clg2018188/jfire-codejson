package com.jfireframework.codejson.methodinfo.impl.write.extra;

import java.lang.reflect.Method;
import com.jfireframework.codejson.function.WriteStrategy;
import com.jfireframework.codejson.methodinfo.impl.write.AbstractWriteMethodInfo;
import com.jfireframework.codejson.util.NameTool;

public class ReturnArrayListMethodInfo extends AbstractWriteMethodInfo
{
    
    public ReturnArrayListMethodInfo(Method method, WriteStrategy strategy, String entityName)
    {
        super(method, strategy, entityName);
        String fieldName = NameTool.getNameFromMethod(method, strategy);
        str = "java.util.ArrayList " + fieldName + " = " + getValue + ";\r\n";
        str += "if(" + fieldName + "!=null)\r\n{\r\n";
        String key = method.getDeclaringClass().getName() + '.' + fieldName;
        if (strategy == null)
        {
            str += "\tcache.append(\"\\\"" + fieldName + "\\\":[\");\r\n";
            str += "\tint size = " + fieldName + ".size();\r\n";
            str += "\tObject valueTmp = null;\r\n";
            str += "\tfor(int i=0;i<size;i++)\r\n";
            str += "\t{\r\n";
            str += "\t\tif((valueTmp=" + fieldName + ".get(i))!=null)\r\n";
            str += "\t\t{\r\n";
            str += "\t\t\tif(valueTmp instanceof String)\r\n";
            str += "\t\t\t{\r\n";
            str += "\t\t\t\tcache.append('\\\"').append((String)valueTmp).append('\\\"');\r\n";
            str += "\t\t\t}\r\n";
            str += "\t\t\telse\r\n";
            str += "\t\t\t{\r\n";
            str += "\t\t\t\tWriterContext.write(valueTmp,cache);\r\n";
            str += "\t\t\t}\r\n";
            str += "\t\t\tcache.append(',');\r\n";
            str += "\t\t}\r\n";
            str += "\t}\r\n";
            str += "\tif(cache.isCommaLast()){cache.deleteLast();}\r\n";
            str += "\tcache.append(\"],\");\r\n";
            str += "}\r\n";
        }
        else
        {
            if (strategy.containsStrategyField(key))
            {
                str += "\tcache.append(\"\\\"" + fieldName + "\\\":\");\r\n";
                str += "\tJsonWriter writer = writeStrategy.getWriterByField(\"" + key + "\");\r\n";
                if (strategy.isUseTracker())
                {
                    str += "\t_$tracker.reset(_$reIndex);\r\n";
                    str += "\tint _$index = _$tracker.indexOf(" + fieldName + ");\r\n";
                    str += "\tif(_$index == -1)\r\n";
                    str += "\t{\r\n";
                    str += "\t\t_$tracker.put(" + fieldName + ",\"" + fieldName + "\",false);\r\n";
                    str += "\t}\r\n";
                    str += "\twriter.write(" + fieldName + ",cache," + entityName + ",_$tracker);\r\n";
                }
                else
                {
                    str += "\twriter.write(" + fieldName + ",cache," + entityName + ",null);\r\n";
                }
                str += "\tcache.append(',');\r\n";
                str += "}\r\n";
            }
            else
            {
                if (strategy.isUseTracker())
                {
                    str += "\t_$tracker.reset(_$reIndex);\r\n";
                    str += "\tint _$index = _$tracker.indexOf(" + fieldName + ");\r\n";
                    str += "\tif(_$index != -1)\r\n";
                    str += "\t{\r\n";
                    str += "\t\tcache.append(\"\\\"" + fieldName + "\\\":\");\r\n";
                    str += "\t\tJsonWriter writer = writeStrategy.getTrackerType(java.util.ArrayList.class);\r\n";
                    str += "\t\tif(writer != null)\r\n";
                    str += "\t\t{\r\n";
                    str += "\t\t\twriter.write(" + fieldName + ",cache," + entityName + ",_$tracker);\r\n";
                    str += "\t\t}\r\n";
                    str += "\t\telse\r\n";
                    str += "\t\t{\r\n";
                    str += "\t\t\tcache.append(\"{\\\"$ref\\\":\\\"\").append(_$tracker.getPath(_$index)).append('\"').append('}');\r\n";
                    str += "\t\t}\r\n";
                    str += "\t}\r\n";
                    str += "\telse\r\n";
                    str += "\t{\r\n";
                    str += "\t\tint _$reIndex1 = _$tracker.put(" + fieldName + ",\""+fieldName+"\",false);\r\n";
                    str += "\t\tcache.append(\"\\\"" + fieldName + "\\\":[\");\r\n";
                    str += "\t\tint size = " + fieldName + ".size();\r\n";
                    str += "\t\tObject valueTmp = null;\r\n";
                    str += "\t\tfor(int i=0;i<size;i++)\r\n";
                    str += "\t\t{\r\n";
                    str += "\t\t\tif((valueTmp=" + fieldName + ".get(i))!=null)\r\n";
                    str += "\t\t\t{\r\n";
                    str += "\t\t\t\tif(valueTmp instanceof String || valueTmp instanceof Number || valueTmp instanceof Boolean)\r\n";
                    str += "\t\t\t\t{\r\n";
                    str += "\t\t\t\t\tcache.append('\\\"').append(valueTmp).append('\\\"');\r\n";
                    str += "\t\t\t\t}\r\n";
                    str += "\t\t\t\telse\r\n";
                    str += "\t\t\t\t{\r\n";
                    str += "\t\t\t\t\t_$tracker.reset(_$reIndex1);\r\n";
                    str += "\t\t\t\t\tint _$index1 = _$tracker.indexOf(valueTmp);\r\n";
                    str += "\t\t\t\t\tif(_$index1 != -1)\r\n";
                    str += "\t\t\t\t\t{\r\n";
                    str += "\t\t\t\t\t\tJsonWriter writer1 = writeStrategy.getTrackerType(valueTmp.getClass());\r\n";
                    str += "\t\t\t\t\t\tif(writer1 != null)\r\n";
                    str += "\t\t\t\t\t\t{\r\n";
                    str += "\t\t\t\t\t\t\twriter1.write(valueTmp,cache," + entityName + ",_$tracker);\r\n";
                    str += "\t\t\t\t\t\t}\r\n";
                    str += "\t\t\t\t\t\telse\r\n";
                    str += "\t\t\t\t\t\t{\r\n";
                    str += "\t\t\t\t\t\t\tcache.append(\"{\\\"$ref\\\":\\\"\").append(_$tracker.getPath(_$index1)).append('\"').append('}');\r\n";
                    str += "\t\t\t\t\t\t}\r\n";
                    str += "\t\t\t\t\t}\r\n";
                    str += "\t\t\t\t\telse\r\n";
                    str += "\t\t\t\t\t{\r\n";
                    str += "\t\t\t\t\t\t_$tracker.put(valueTmp,\"[\"+i+\"]\",true);\r\n";
                    str += "\t\t\t\t\t\twriteStrategy.getWriter(valueTmp.getClass()).write(valueTmp,cache," + entityName + ",_$tracker);\r\n";
                    str += "\t\t\t\t\t}\r\n";
                    str += "\t\t\t\t}\r\n";
                    str += "\t\t\t\tcache.append(',');\r\n";
                    str += "\t\t\t}\r\n";
                    str += "\t\t}\r\n";
                    str += "\t}\r\n";
                    str += "\tif(cache.isCommaLast()){cache.deleteLast();}\r\n";
                    str += "\tcache.append(\"],\");\r\n";
                    str += "}\r\n";
                }
                else
                {
                    str += "\tcache.append(\"\\\"" + fieldName + "\\\":[\");\r\n";
                    str += "\tint size = " + fieldName + ".size();\r\n";
                    str += "\tObject valueTmp = null;\r\n";
                    str += "\tfor(int i=0;i<size;i++)\r\n";
                    str += "\t{\r\n";
                    str += "\t\tif((valueTmp=" + fieldName + ".get(i))!=null)\r\n";
                    str += "\t\t{\r\n";
                    str += "\t\t\tif(valueTmp instanceof String || valueTmp instanceof Number || valueTmp instanceof Boolean)\r\n";
                    str += "\t\t\t{\r\n";
                    str += "\t\t\t\tcache.append('\\\"').append(valueTmp).append('\\\"');\r\n";
                    str += "\t\t\t}\r\n";
                    str += "\t\t\telse\r\n";
                    str += "\t\t\t{\r\n";
                    str += "\t\t\t\twriteStrategy.getWriter(valueTmp.getClass()).write(valueTmp,cache," + entityName + ",null);\r\n";
                    str += "\t\t\t}\r\n";
                    str += "\t\t\tcache.append(',');\r\n";
                    str += "\t\t}\r\n";
                    str += "\t}\r\n";
                    str += "\tif(cache.isCommaLast()){cache.deleteLast();}\r\n";
                    str += "\tcache.append(\"],\");\r\n";
                    str += "}\r\n";
                }
            }
        }
    }
    
}
