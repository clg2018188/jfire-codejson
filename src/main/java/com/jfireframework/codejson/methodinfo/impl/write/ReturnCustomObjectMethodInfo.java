package com.jfireframework.codejson.methodinfo.impl.write;

import java.lang.reflect.Method;
import com.jfireframework.baseutil.smc.SmcHelper;
import com.jfireframework.codejson.function.WriteStrategy;
import com.jfireframework.codejson.util.NameTool;

public class ReturnCustomObjectMethodInfo extends AbstractWriteMethodInfo
{
    
    public ReturnCustomObjectMethodInfo(Method method, WriteStrategy strategy, String entityName)
    {
        super(method, strategy, entityName);
        Class<?> returnType = method.getReturnType();
        String fieldName = NameTool.getNameFromMethod(method, strategy);
        str = SmcHelper.getTypeName(returnType) + " " + fieldName + " = " + getValue + ";\r\n";
        str += "if(" + fieldName + "!=null)\r\n{\r\n";
        str += "\tcache.append(\"\\\"" + fieldName + "\\\":\");\r\n";
        String key = method.getDeclaringClass().getName() + '.' + fieldName;
        if (strategy != null)
        {
            if (strategy.containsStrategyField(key))
            {
                str += "\tJsonWriter writer = writeStrategy.getWriterByField(\"" + key + "\");\r\n";
                if (strategy.isUseTracker())
                {
                    str += "\t_$tracker.reset(_$reIndex);\r\n";
                    str += "\tint _$index = _$tracker.indexOf(" + fieldName + ");\r\n";
                    str += "\tif(_$index == -1)\r\n";
                    str += "\t{\r\n";
                    str += "\t\t_$tracker.put(" + fieldName + ",\"" + fieldName + "\",false);\r\n";
                    str += "\t}\r\n";
                    str += "\twriter.write(" + fieldName + ",cache," + entityName + ",_$tracker);\r\n";
                }
                else
                {
                    str += "\twriter.write(" + fieldName + ",cache," + entityName + ",null);\r\n";
                    str += "\tcache.append(',');\r\n";
                    str += "}\r\n";
                }
            }
            else
            {
                str += "\tJsonWriter writer = writeStrategy.getWriter(" + fieldName + ".getClass());\r\n";
                if (strategy.isUseTracker())
                {
                    str += "\t_$tracker.reset(_$reIndex);\r\n";
                    str += "\tint _$index = _$tracker.indexOf(" + fieldName + ");\r\n";
                    str += "\tif(_$index != -1)\r\n\t{\r\n";
                    str += "\t\twriter = writeStrategy.getTrackerType(" + fieldName + ".getClass());\r\n";
                    str += "\t\tif(writer != null )\r\n";
                    str += "\t\t{\r\n";
                    str += "\t\t\twriter.write(" + fieldName + ",cache," + entityName + ",_$tracker);\r\n";
                    str += "\t\t}\r\n";
                    str += "\t\telse\r\n";
                    str += "\t\t{\r\n";
                    str += "\t\t\tcache.append(\"{\\\"$ref\\\":\\\"\").append(_$tracker.getPath(_$index)).append('\"').append('}');\r\n";
                    str += "\t\t}\r\n";
                    str += "\t}\r\n";
                    str += "\telse\r\n";
                    str += "\t{\r\n";
                    str += "\t\t_$tracker.put(" + fieldName + ",\"" + fieldName + "\",false);\r\n";
                    str += "\t\twriter.write(" + fieldName + ",cache," + entityName + ",_$tracker);\r\n";
                    str += "\t}\r\n";
                }
                else
                {
                    str += "\twriter.write(" + fieldName + ",cache," + entityName + ",null);\r\n";
                }
                str += "\tcache.append(',');\r\n";
                str += "}\r\n";
            }
        }
        else
        {
            str += "\tWriterContext.write(" + fieldName + ",cache);\r\n";
            str += "\tcache.append(',');\r\n";
            str += "}\r\n";
        }
    }
    
}
