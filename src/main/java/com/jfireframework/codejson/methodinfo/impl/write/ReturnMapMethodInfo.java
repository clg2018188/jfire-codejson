package com.jfireframework.codejson.methodinfo.impl.write;

import java.lang.reflect.Method;
import com.jfireframework.codejson.function.WriteStrategy;
import com.jfireframework.codejson.function.impl.write.wrapper.StringWriter;
import com.jfireframework.codejson.util.NameTool;

public class ReturnMapMethodInfo extends AbstractWriteMethodInfo
{
    
    public ReturnMapMethodInfo(Method method, WriteStrategy strategy, String entityName)
    {
        super(method, strategy, entityName);
        String fieldName = NameTool.getNameFromMethod(method, strategy);
        str = "Map " + fieldName + " = " + getValue + ";\r\n";
        str += "if(" + fieldName + "!=null)\r\n{\r\n";
        String key = method.getDeclaringClass().getName() + '.' + fieldName;
        if (strategy != null)
        {
            if (strategy.containsStrategyField(key))
            {
                str += "\tcache.append(\"\\\"" + fieldName + "\\\":\");\r\n";
                str += "\tJsonWriter writer = writeStrategy.getWriterByField(\"" + key + "\");\r\n";
                if (strategy.isUseTracker())
                {
                    str += "\t_$tracker.reset(_$reIndex);\r\n";
                    str += "\tint _$index = _$tracker.indexOf(" + fieldName + ");\r\n";
                    str += "\tif(_$index == -1)\r\n";
                    str += "\t{\r\n";
                    str += "\t\t_$tracker.put(" + fieldName + ",\"" + fieldName + "\",false);\r\n";
                    str += "\t}\r\n";
                    str += "\telse\r\n";
                    str += "\t{\r\n";
                    str += "\t\twriter.write(" + fieldName + ",cache," + entityName + ",_$tracker);\r\n";
                    str += "\t}\r\n";
                    str += "}\r\n";
                }
                else
                {
                    str += "\twriter.write(" + fieldName + ",cache," + entityName + ",null);\r\n";
                    str += "\tcache.append(',');\r\n";
                    str += "}\r\n";
                }
            }
            else
            {
                if (strategy.isUseTracker())
                {
                    str += "\t_$tracker.reset(_$reIndex);\r\n";
                    str += "\tint _$index = _$tracker.indexOf(" + fieldName + ");\r\n";
                    str += "\tif(_$index != -1)\r\n\t{\r\n";
                    str += "\t\tJsonWriter writer = writeStrategy.getTrackerType(" + fieldName + ".getClass());\r\n";
                    str += "\t\tif(writer != null)\r\n";
                    str += "\t\t{\r\n";
                    str += "\t\t\twriter.write(" + fieldName + ",cache," + entityName + ",_$tracker);\r\n";
                    str += "\t\t}\r\n";
                    str += "\t\telse\r\n";
                    str += "\t\t{\r\n";
                    str += "\t\t\tcache.append(\"{\\\"$ref\\\":\\\"\").append(_$tracker.getPath(_$index)).append('\"').append('}');\r\n";
                    str += "\t\t}\r\n";
                    str += "\t}\r\n";
                    str += "\telse\r\n";
                    str += "\t{\r\n";
                    str += "\t\tint _$reIndex1 = _$tracker.put(" + fieldName + ",\"" + fieldName + "\",false);\r\n";
                    str += "\t\tcache.append(\"\\\"" + fieldName + "\\\":{\");\r\n";
                    str += "\t\tSet entries = " + fieldName + ".entrySet();\r\n";
                    str += "\t\tIterator it = entries.iterator();\r\n";
                    str += "\t\tjava.util.Map.Entry entry = null;\r\n";
                    str += "\t\twhile(it.hasNext())\r\n";
                    str += "\t\t{\r\n";
                    str += "\t\t\tentry = (Entry)it.next();\r\n";
                    str += "\t\t\tif(entry.getKey()!=null && entry.getValue()!=null)\r\n";
                    str += "\t\t\t{\r\n";
                    str += "\t\t\t\tif(entry.getKey() instanceof String)\r\n";
                    str += "\t\t\t\t{\r\n";
                    if (strategy.getWriter(String.class) instanceof StringWriter)
                    {
                        str += "\t\t\t\t\tcache.append('\\\"').append((String)entry.getKey()).append(\"\\\":\");\r\n";
                    }
                    else
                    {
                        str += "\t\t\t\t\twriteStrategy.getWriter(String.class).write(entry.getKey(),cache," + entityName + ",_$tracker);\r\n";
                        str += "\t\t\t\t\tcache.append(':');\r\n";
                    }
                    str += "\t\t\t\t}\r\n";
                    str += "\t\t\t\telse\r\n";
                    str += "\t\t\t\t{\r\n";
                    str += "\t\t\t\t\tcache.append('\"');\r\n";
                    str += "\t\t\t\t\twriteStrategy.getWriter(entry.getKey().getClass()).write(entry.getKey(),cache," + entityName + ",_$tracker);\r\n";
                    str += "\t\t\t\t\tcache.append(\"\\\":\");\r\n";
                    str += "\t\t\t\t}\r\n";
                    str += "\t\t\t\tif(entry.getValue() instanceof String)\r\n";
                    str += "\t\t\t\t{\r\n";
                    if (strategy.getWriter(String.class) instanceof StringWriter == false)
                    {
                        str += "\t\t\t\twriteStrategy.getWriter(entry.getValue().getClass()).write(entry.getValue(),cache," + entityName + ",_$tracker);\r\n";
                    }
                    else
                    {
                        str += "\t\t\t\t\tcache.append('\\\"').append((String)entry.getValue()).append('\\\"');\r\n";
                    }
                    str += "\t\t\t\t}\r\n";
                    str += "\t\t\t\telse\r\n";
                    str += "\t\t\t\t{\r\n";
                    str += "\t\t\t\t\t_$tracker.reset(_$reIndex1);\r\n";
                    str += "\t\t\t\t\tint _$index1 = _$tracker.indexOf(entry.getValue());\r\n";
                    str += "\t\t\t\t\tif(_$index1 != -1)\r\n";
                    str += "\t\t\t\t\t{\r\n";
                    str += "\t\t\t\t\t\t JsonWriter writer = writeStrategy.getTrackerType(entry.getValue().getClass());\r\n";
                    str += "\t\t\t\t\t\tif(writer != null)\r\n";
                    str += "\t\t\t\t\t\t{\r\n";
                    str += "\t\t\t\t\t\t\twriter.write(entry.getValue(),cache," + entityName + ",_$tracker);\r\n";
                    str += "\t\t\t\t\t\t}\r\n";
                    str += "\t\t\t\t\t\telse\r\n";
                    str += "\t\t\t\t\t\t{\r\n";
                    str += "\t\t\t\t\t\t\tcache.append(\"{\\\"$ref\\\":\\\"\").append(_$tracker.getPath(_$index1)).append('\"').append('}');\r\n";
                    str += "\t\t\t\t\t\t}\r\n";
                    str += "\t\t\t\t\t}\r\n";
                    str += "\t\t\t\t\telse\r\n";
                    str += "\t\t\t\t\t{\r\n";
                    str += "\t\t\t\t\t\t_$tracker.put(entry.getValue(),entry.getKey().toString(),false);\r\n";
                    str += "\t\t\t\t\t\twriteStrategy.getWriter(entry.getValue().getClass()).write(entry.getValue(),cache," + entityName + ",_$tracker);\r\n";
                    str += "\t\t\t\t\t}\r\n";
                    str += "\t\t\t\t}\r\n";
                    str += "\t\t\t\tcache.append(',');\r\n";
                    str += "\t\t\t}\r\n";
                    str += "\t\t}\r\n";
                    str += "\t\tif(cache.isCommaLast()){cache.deleteLast();}\r\n";
                    str += "\t\tcache.append(\"},\");\r\n";
                    str += "\t}\r\n";
                    str += "}\r\n";
                    
                }
                else
                {
                    str += "\tcache.append(\"\\\"" + fieldName + "\\\":{\");\r\n";
                    str += "\tSet entries = " + fieldName + ".entrySet();\r\n";
                    str += "\tIterator it = entries.iterator();\r\n";
                    str += "\tjava.util.Map.Entry entry = null;\r\n";
                    str += "\twhile(it.hasNext())\r\n\t{\r\n";
                    str += "\t\tentry = (Entry)it.next();\r\n";
                    str += "\t\tif(entry.getKey()!=null && entry.getValue()!=null)\r\n";
                    str += "\t\t{\r\n";
                    str += "\t\t\tif(entry.getKey() instanceof String)\r\n";
                    str += "\t\t\t{\r\n";
                    if (strategy.getWriter(String.class) instanceof StringWriter)
                    {
                        str += "\t\t\t\tcache.append('\\\"').append((String)entry.getKey()).append(\"\\\":\");\r\n";
                    }
                    else
                    {
                        str += "\t\t\t\twriteStrategy.getWriter(String.class).write(entry.getKey(),cache," + entityName + ",null);\r\n";
                        str += "\t\t\t\tcache.append(':');\r\n";
                    }
                    str += "\t\t\t}\r\n";
                    str += "\t\t\telse\r\n";
                    str += "\t\t\t{\r\n";
                    str += "\t\t\t\tcache.append('\"');\r\n";
                    str += "\t\t\t\twriteStrategy.getWriter(entry.getKey().getClass()).write(entry.getKey(),cache," + entityName + ",_$tracker);\r\n";
                    str += "\t\t\t\tcache.append(\"\\\":\");\r\n";
                    str += "\t\t\t}\r\n";
                    if (strategy.getWriter(String.class) instanceof StringWriter == false)
                    {
                        str += "\t\t\twriteStrategy.getWriter(entry.getValue().getClass()).write(entry.getValue(),cache," + entityName + ",null);\r\n";
                    }
                    else
                    {
                        str += "\t\t\tif(entry.getValue() instanceof String)\r\n";
                        str += "\t\t\t{\r\n";
                        str += "\t\t\t\tcache.append('\\\"').append((String)entry.getValue()).append('\\\"');\r\n";
                        str += "\t\t\t}\r\n";
                        str += "\t\t\telse\r\n";
                        str += "\t\t\t{\r\n";
                        str += "\t\t\t\twriteStrategy.getWriter(entry.getValue().getClass()).write(entry.getValue(),cache," + entityName + ",null);\r\n";
                        str += "\t\t\t}\r\n";
                    }
                    str += "\t\t\tcache.append(',');\r\n";
                    str += "\t\t}\r\n";
                    str += "\t}\r\n";
                    str += "if(cache.isCommaLast()){cache.deleteLast();}\r\n";
                    str += "cache.append(\"},\");\r\n";
                    str += "}\r\n";
                }
            }
        }
        else
        {
            str += "\tcache.append(\"\\\"" + fieldName + "\\\":{\");\r\n";
            str += "\tSet entries = " + fieldName + ".entrySet();\r\n";
            str += "\tIterator it = entries.iterator();\r\n";
            str += "\tjava.util.Map.Entry entry = null;\r\n";
            str += "\twhile(it.hasNext())\r\n\t{\r\n";
            str += "\t\tentry =(Entry) it.next();\r\n";
            str += "\t\tif(entry.getKey()!=null && entry.getValue()!=null)\r\n";
            str += "\t\t{\r\n";
            str += "\t\t\tif(entry.getKey() instanceof String)\r\n";
            str += "\t\t\t{\r\n";
            str += "\t\t\t\tcache.append('\\\"').append((String)entry.getKey()).append(\"\\\":\");\r\n";
            str += "\t\t\t}\r\n";
            str += "\t\t\telse\r\n";
            str += "\t\t\t{\r\n";
            str += "\t\t\t\tcache.append('\"');\r\n";
            str += "\t\t\t\tWriterContext.write(entry.getKey(),cache);\r\n";
            str += "\t\t\t\tcache.append(\"\\\":\");\r\n";
            str += "\t\t\t}\r\n";
            str += "\t\t\tif(entry.getValue() instanceof String)\r\n";
            str += "\t\t\t{\r\n";
            str += "\t\t\t\tcache.append('\\\"').append((String)entry.getValue()).append('\\\"');\r\n";
            str += "\t\t\t}\r\n";
            str += "\t\t\telse\r\n";
            str += "\t\t\t{\r\n";
            str += "\t\t\t\tWriterContext.write(entry.getValue(),cache);\r\n";
            str += "\t\t\t}\r\n";
            str += "\t\t\tcache.append(',');\r\n";
            str += "\t\t}\r\n";
            str += "\t}\r\n";
            str += "\tif(cache.isCommaLast()){cache.deleteLast();}\r\n";
            str += "\tcache.append(\"},\");\r\n";
            str += "}\r\n";
        }
    }
}
